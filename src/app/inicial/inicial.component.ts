import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { CardgetterService } from '../cardgetter.service';

@Component({
  selector: 'app-inicial',
  templateUrl: './inicial.component.html',
  styleUrls: ['./inicial.component.sass'],
  providers: [CardgetterService]
})

export class InicialComponent implements OnInit {

  @ViewChild('inputName', {read: ElementRef}) inputName: ElementRef;

  constructor(private _cardGetter: CardgetterService,
              private _router: Router) { }

  AllCards:any;
  cards:any;
  requests:any;
  searchName:any;
  
  ngOnInit(): void {
  	this._cardGetter.getCards().subscribe((data_)=>{
      if(data_){
        console.log(data_)
        let cards_ = data_.cards.sort((a,b) => a.name.localeCompare(b.name))
        this.AllCards = [...cards_];
        this.cards = [...cards_];
      }
    })
  }

  clickCard(id:any):void {
    console.log(id)
    this._router.navigateByUrl('/card?id=' + id);
  }

  clickSearch(){
    this._cardGetter.getCardsByName(this.searchName).subscribe((data_)=>{
      if(data_){
        console.log(data_)
        let cards_ = data_.cards.sort((a,b) => a.name.localeCompare(b.name))
        this.cards = [...cards_];
      }
    })
  }

}
