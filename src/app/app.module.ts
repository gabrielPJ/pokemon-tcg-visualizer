import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicialComponent } from './inicial/inicial.component';
import { CardComponent } from './card/card.component';

import { CardgetterService } from './cardgetter.service';

@NgModule({
  declarations: [
    AppComponent,
    InicialComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [CardgetterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
