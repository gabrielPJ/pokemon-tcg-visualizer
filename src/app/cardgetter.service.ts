import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import { expand, map, reduce } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CardgetterService {

  constructor(private http: HttpClient) { }

  ngOnInit(){
  }
  /* TOO MANY CARDS
  getCardsByPage(page = 1): Observable<any>{
  	console.log('getCards', page)
  	return this.http.get('https://api.pokemontcg.io/v1/cards?page='+page);
  }
  
  getCards(): Observable<any>{
    let page = 1;
    return this.getCardsByPage().pipe(
      expand((response: any) => {
        if(response.cards && response.cards.length > 0){
          return this.getCardsByPage(++page);
        }else{
          return empty()
        }
      }),
      reduce((acc:any, x:any) => {
        console.log(acc, x)
        let cards_1 = acc.cards ? [...acc.cards] : [];
        let cards_2 = x.cards ? [...x.cards] : [];
        return {cards: [...cards_1, ...cards_2]} 
      }, [])
    );
  }*/ 
  getCards(page = 1): Observable<any>{
    console.log('getCards', page)
    return this.http.get('https://api.pokemontcg.io/v1/cards?page='+page);
  }
  
  getCardById(id:String): Observable<any>{
    console.log('getCardById', id)
    return this.http.get('https://api.pokemontcg.io/v1/cards?id='+id);
  }
  
  getCardsByName(name:String): Observable<any>{
    console.log('getCardsByName', name)
    return this.http.get('https://api.pokemontcg.io/v1/cards?name='+name);
  }
}
