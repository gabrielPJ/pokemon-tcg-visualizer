import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InicialComponent } from './inicial/inicial.component';
import { CardComponent } from './card/card.component';

const routes: Routes = [
	{
		path: '',
		component: InicialComponent
	},
	{
		path: 'card',
		component: CardComponent
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
