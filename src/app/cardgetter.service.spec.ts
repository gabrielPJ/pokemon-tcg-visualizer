import { TestBed } from '@angular/core/testing';

import { CardgetterService } from './cardgetter.service';

describe('CardgetterService', () => {
  let service: CardgetterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CardgetterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
