import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Observable } from 'rxjs';
import { CardgetterService } from '../cardgetter.service';

@Component({
	selector: 'app-card',
	templateUrl: './card.component.html',
	styleUrls: ['./card.component.sass'],
	providers: [CardgetterService]
})

export class CardComponent implements OnInit {

	constructor(private _cardGetter: CardgetterService,
				private _route: ActivatedRoute) { }

	card:any;

	ngOnInit(): void {
		this._route.queryParams.subscribe((params: Params) => {
			console.log('params:', params)
			if(params.id){
			  	this._cardGetter.getCardById(params.id).subscribe((data_)=>{
			      console.log(data_, data_.cards[0])
			      this.card = {...data_.cards[0]};
			    })
			}
		})
	}

}
